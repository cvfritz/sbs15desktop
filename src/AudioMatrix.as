package {
	import Devices.AudioDevice;
	import Devices.DBXZonePro;
	import Devices.HiQNet;
	
	import flash.errors.IOError;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.SharedObject;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	public class AudioMatrix extends EventDispatcher {
		
		public var deviceName: String="";
		public var connected: Boolean=false;
		public var address: String="";
		public var port: int=0;
		public var nodeAddress: int=0;
		
		private var socket: Socket = null;
		private var routeMap: Array = new Array();
		private var volumeMap: Array = new Array();
		private var unmuteMap: Array = new Array();
		private var pingTimer: Timer = new Timer(800);
		private var discoTimer: Timer = new Timer(10000);
		private var zoneCodes: Array = new Array(); 
		private var volumeValues: Array = new Array(0);
		private var mapXML:XML;
		private var fileStream:FileStream;
		private var type:String = "";
		
		private var device:AudioDevice;
		
		public function AudioMatrix() {
			
//			pingTimer.addEventListener(TimerEvent.TIMER, sendPing) ;
//			discoTimer.addEventListener(TimerEvent.TIMER, sendDisco) ;
			
			try {
				var file:File = new File("C:\\sportsbarsportsbar\\AudioMatrix_"+deviceName + ".xml");
				fileStream = new FileStream();
				fileStream.open(file, FileMode.UPDATE);
				mapXML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
			} catch (e:IOError) {
				trace (e.message);
			}
			volumeValues[0]=0;			
			for(var i: int=1; i<=21; i++) {
				volumeValues[i] = 15+(i-1)*20;	
			}				
		}
		
		private function saveXML():void {
			fileStream.writeUTFBytes(mapXML.toString());				
		}

		private static var d2h: Array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F") ;
		private function bToHex(b: int): String {
			var h: int=(b>>4)&0x0f;
			var l: int=(b&0x0f);
			return d2h[h] + d2h[l];
		}
		
		private function baToHex(ba: ByteArray): String {
			ba.position=0;
			var str: String="";
			for(var i: int=0; i<ba.length; i++)
				str+=bToHex(ba.readByte());
			return str;
		}
		
		private function hexToInt(s: String): int {
			var ti: int=0 ;
			for(var i: int=0; i<s.length; i++) {
				ti = ti << 4 ;
				var h: int = s.charCodeAt(i);
				if(s.charAt(i)>='0' && s.charAt(i)<='9')
					h-="0".charCodeAt(0);
				else if (s.charAt(i)>='a' && s.charAt(i)<='f')
					h = 10 + h - "a".charCodeAt(0);
				else if (s.charAt(i)>='A' && s.charAt(i)<='F')
					h = 10 + h - "A".charCodeAt(0);
				ti |= h;
			}
			return ti;
		}
		
		public function fromXml(xml: XML): void {
			
			if(xml==null)
				return;
				
			deviceName=xml.deviceName;
			address=xml.ipAddress;
			port=xml.port;
			nodeAddress=xml.nodeAddress;
			type = xml.type;
			var xl: XMLList = xml.zones.zone;
			for(var i: int=0; i<xl.length(); i++) {
				var tx: XML = xl[i];
				zoneCodes[i]=tx;
			}
			
			Logger.trase("AudioMatrix fromXml, address: " + address + ", port: " + port + ", type: " + type);
			
			//CVF Modified!!!
			if (type == "Zone Pro") device = new DBXZonePro(deviceName, 6, xml);
			else device = new HiQNet(deviceName, 20, xml);
			if(address!="" && port!=0)
				connect();
			
			routeMap = SharedObject.getLocal("AudioMatrix_"+deviceName).data.routeMap;
			if(routeMap==null)
				routeMap=new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.routeMap=routeMap;
			dispatchEvent(new Event("mapUpdated"));
			
			volumeMap = SharedObject.getLocal("AudioMatrix_"+deviceName).data.volumeMap;
			if(volumeMap==null)
				volumeMap=new Array(-10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10);
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.volumeMap=volumeMap;
			
			unmuteMap = SharedObject.getLocal("AudioMatrix_"+deviceName).data.unmuteMap;
			if(unmuteMap==null)
				unmuteMap=new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.unmuteMap=unmuteMap;
			
			for(var i: int=0; i<volumeMap.length; i++) {
				//setVolume(i, volumeMap[i]);
			}
		}
			//This section is replacing shared object for the desktop version
			//var mapXML:XML = null;
			//var existed:Boolean;
			//try {
	/*			var file:File = new File("C:\\sportsbar\\AudioMatrix_"+deviceName + ".xml");
				existed = file.exists;
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.UPDATE);
				//FIXME!!!
				//mapXML = XML(stream.readUTFBytes(stream.bytesAvailable));
			} catch (e:IOError) {
				mapXML = null;
				trace (e.message);
			} finally {
				stream.close();
			}
			
			if (mapXML != null) {
				if(!existed)
					routeMap=new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				else 
					
				routeMap = mapXML.routeMap;
				mapXML.routeMap=routeMap;
				dispatchEvent(new Event("mapUpdated"));
				
				if(!existed)
					volumeMap=new Array(30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30);
				else 
					volumeMap = mapXML.volumeMap;
				mapXML.volumeMap=volumeMap;
				
				if(!existed)
					unmuteMap=new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				else 
					unmuteMap = mapXML.unmuteMap;
				mapXML.unmuteMap=unmuteMap;
			}
			for(var i: int=0; i<volumeMap.length; i++) {
				//sendSetVolume(i);
			}
		}
	*/	
//		private function socketSend(ba: ByteArray): void {
//			Logger.trase("AudioMatrix socketSend, data: F0" + baToHex(ba));
//			ba.position=0;
//			socket.writeByte(0xf0);
//			socket.writeBytes(ba, 0, ba.length);
//			socket.flush();
//		}
		
		private function connect(): void {
			if(address=="" || port<=0)
				return;	
			//****Commented out CVF 10/2/12. Using new mechanism for communication.
//			Logger.trase("AudioMatrix connect, address: " + address + ", port: " + port);
//			if(socket==null) {
//				socket=new Socket();
//				socket.addEventListener(IOErrorEvent.IO_ERROR, onSocketError);
//				socket.addEventListener(Event.CONNECT, onSocketConnected);
//				socket.addEventListener(Event.CLOSE, onSocketClosed);
//				socket.addEventListener(ProgressEvent.SOCKET_DATA, onSocketData);
//				socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocketSecurityError);
//			}
//			socket.connect(address, port);
			device.connect(address, port);
			
		}
		
//		private function onSocketSecurityError(obj: Object): void {
//			Logger.trase("AudioMatrix onSocketSecurityError, obj: " + obj); 
//			connected=false;
//		}
//		
//		private function onSocketError(obj: Object): void {
//			Logger.trase("AudioMatrix onSocketError,obj: " + obj); 
//			connected=false;
//			var t: Timer = new Timer(10000);
//			t.repeatCount=1;
//			t.addEventListener(TimerEvent.TIMER, function(event: TimerEvent): void{
//				connect();
//			});
//			t.start();
//		}
//		
//		private function onSocketConnected(obj: Object): void {
//			Logger.trase("AudioMatrix onSocketConnected, obj: " + obj); 
//			connected=true;
//			pingTimer.start();
//			//discoTimer.start();
//		}
//		
//		private function onSocketClosed(obj: Object): void {
//			Logger.trase("AudioMatrix onSocketClosed, obj: " + obj); 
//			connected=false;
//			var t: Timer = new Timer(1000);
//			t.repeatCount=1;
//			t.addEventListener(TimerEvent.TIMER, function(event){
//				connect();	
//			});
//			t.start();
//		}
//		
//		private var incomingBa: ByteArray = new ByteArray() ;
//		private function onSocketData(obj: Object): void {
//			socket.readBytes(incomingBa, 0, obj.bytesLoaded);
//			Logger.trase("AudioMatrix onSocketData, data: " +  baToHex(incomingBa));
//		}
//		
//		private static var ccit:Array = new Array(0x00,
//		0x5E,0xBC,0xE2,0x61,0x3F,0xDD,0x83,0xC2,0x9C,0x7E,0x20,0xA3,0xFD,0x1F,0x41,
//		0x9D,0xC3,0x21,0x7F,0xFC,0xA2,0x40,0x1E,0x5F,0x01,0xE3,0xBD,0x3E,0x60,0x82,0xDC,
//		0x23,0x7D,0x9F,0xC1,0x42,0x1C,0xFE,0xA0,0xE1,0xBF,0x5D,0x03,0x80,0xDE,0x3C,0x62,
//		0xBE,0xE0,0x02,0x5C,0xDF,0x81,0x63,0x3D,0x7C,0x22,0xC0,0x9E,0x1D,0x43,0xA1,0xFF,
//		0x46,0x18,0xFA,0xA4,0x27,0x79,0x9B,0xC5,0x84,0xDA,0x38,0x66,0xE5,0xBB,0x59,0x07,
//		0xDB,0x85,0x67,0x39,0xBA,0xE4,0x06,0x58,0x19,0x47,0xA5,0xFB,0x78,0x26,0xC4,0x9A,
//		0x65,0x3B,0xD9,0x87,0x04,0x5A,0xB8,0xE6,0xA7,0xF9,0x1B,0x45,0xC6,0x98,0x7A,0x24,
//		0xF8,0xA6,0x44,0x1A,0x99,0xC7,0x25,0x7B,0x3A,0x64,0x86,0xD8,0x5B,0x05,0xE7,0xB9,
//		0x8C,0xD2,0x30,0x6E,0xED,0xB3,0x51,0x0F,0x4E,0x10,0xF2,0xAC,0x2F,0x71,0x93,0xCD,
//		0x11,0x4F,0xAD,0xF3,0x70,0x2E,0xCC,0x92,0xD3,0x8D,0x6F,0x31,0xB2,0xEC,0x0E,0x50,
//		0xAF,0xF1,0x13,0x4D,0xCE,0x90,0x72,0x2C,0x6D,0x33,0xD1,0x8F,0x0C,0x52,0xB0,0xEE,
//		0x32,0x6C,0x8E,0xD0,0x53,0x0D,0xEF,0xB1,0xF0,0xAE,0x4C,0x12,0x91,0xCF,0x2D,0x73,
//		0xCA,0x94,0x76,0x28,0xAB,0xF5,0x17,0x49,0x08,0x56,0xB4,0xEA,0x69,0x37,0xD5,0x8B,
//		0x57,0x09,0xEB,0xB5,0x36,0x68,0x8A,0xD4,0x95,0xCB,0x29,0x77,0xF4,0xAA,0x48,0x16,
//		0xE9,0xB7,0x55,0x0B,0x88,0xD6,0x34,0x6A,0x2B,0x75,0x97,0xC9,0x4A,0x14,0xF6,0xA8,
//		0x74,0x2A,0xC8,0x96,0x15,0x4B,0xA9,0xF7,0xB6,0xE8,0x0A,0x54,0xD7,0x89,0x6B,0x35);
//		private function checksum(ba: ByteArray): int {
//			var cc: int = 0xff;
//			ba.position=0;
//			while(ba.bytesAvailable>0) {
//				var t: int = ba.readByte()&0xff;
//				cc = ccit[cc ^ t];
//			}
//			return cc;
//		}
//		
//		private function sendPing(event: Event): void {
//			if(!connected)
//				return ;
//			Logger.trase("AudioMatrix sendPing");
//			var ba: ByteArray = new ByteArray();
//			ba.writeByte(0x8C);
//			socketSend(ba); 
//		}
//		
//		private function sendDisco(event: Event): void {
//			if(!connected)
//				return ;
//			Logger.trase("AudioMatrix sendDisco");
//			var tba: Array = new Array(0x64, 0x00, 0x01, 0x00, 0x00, 0x00, 0x17, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x33, 0x20);				
//			var ba: ByteArray = new ByteArray();
//			for(var i: int=0; i<tba.length; i++)
//				ba.writeByte(tba[i]);
//			socketSend(ba); 
//		}
//					
		public function getRoute(mxOutPort: int): int {
			return routeMap[mxOutPort];
		}
		
		public function setRoute(mxInPort: int, mxOutPort: int): void {
			Logger.trase("AudioMatrix setRoute, mxInport: " +  mxInPort + ", mxOutPort: " + mxOutPort);
			routeMap[mxOutPort]=mxInPort;
			//CVF Replacing shared object with local xml
			//mapXML.routeMap = routeMap;
			//saveXML();
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.routeMap = routeMap;
			//if(mxInPort<=0 || mxOutPort<=0)
			//	return;
			//sendSetRoute(mxOutPort);
			device.setRoute(mxOutPort, mxInPort);
		}
		
		public function getRouteMap(): Array {
			return routeMap;
		}
		
		public function setRouteMap(rm: Array): void {
			for(var i: int=0; i<rm.length; i++) {
				device.setRoute(i, rm[i]);
			}
		}
		
		public function getVolume(mxOutPort: int): int {
			return volumeMap[mxOutPort];
		}
		
		public function getVolumeMap(): Array {
			return volumeMap;
		}
		
		public function setVolume(mxOutPort: int, volume: int): void {
			Logger.trase("AudioMatrix setVolume, mxOutPort: " +  mxOutPort + ", volume: " + volume);
			volumeMap[mxOutPort]=volume;
			//CVF Replacing shared object with local xml
	/*		try {
				var file:File = new File("C:\\sportsbar\\AudioMatrix_"+deviceName);
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.UPDATE);
				var audioXML:XML = XML(stream.readUTFBytes(stream.bytesAvailable));
				audioXML.volumeMap = volumeMap;
				stream.writeUTFBytes(audioXML.toString());
			} catch (e:IOError) {
				trace (e.message);
			} finally {
				stream.close();
			}
			mapXML.volumeMap = volumeMap;
	saveXML();
*/			SharedObject.getLocal("AudioMatrix_"+deviceName).data.volumeMap = volumeMap;
			//sendSetVolume(mxOutPort);
			device.setVolume(mxOutPort, volume);
		}
		
		public function mute(mxOutPort: int): void {
			Logger.trase("AudioMatrix mute, mxOutPort: " +  mxOutPort);
			unmuteMap[mxOutPort] = getVolume(mxOutPort);
			//mapXML.unmuteMap = unmuteMap; AKR
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.unmuteMap = unmuteMap;
			setVolume(mxOutPort, -120);
			//device.setMute(mxOutPort);
		}
		
		public function unmute(mxOutPort: int) : void {
			Logger.trase("AudioMatrix unmute, mxOutPort: " +  mxOutPort);
			setVolume(mxOutPort, unmuteMap[mxOutPort]);  //This would set volume back after unmute
			unmuteMap[mxOutPort] = -120;
			device.setMute(mxOutPort);
			//mapXML.unmuteMap = unmuteMap;  AKR
			SharedObject.getLocal("AudioMatrix_"+deviceName).data.unmuteMap = unmuteMap;
		}
		
		public function isMuted(mxOutPort: int): Boolean {
			return volumeMap[mxOutPort]==-120 && unmuteMap[mxOutPort]!=-120;
			//return device._mutes[mxOutPort] == "Muted" ? true : false;
		}

//		private function sendSetRoute(mxOutPort: int): void {
//			
//			if(!connected)
//				return;
//		
//			var mxInPort: int = routeMap[mxOutPort];
//		
//			var ba: ByteArray = new ByteArray();
//			ba.writeByte(0x64);
//			ba.writeByte(0x00);
//			ba.writeByte(0x01);
//			
//			ba.writeByte(0x00); // ulong length
//			ba.writeByte(0x00);
//			ba.writeByte(0x00);
//			ba.writeByte(0x1B);
//			
//			ba.writeByte(0x00); // source device
//			ba.writeByte(0x33);
//		
//			var o: Object = zoneCodes[mxOutPort]; 
//			ba.writeByte(o.b3); // b values backwards
//			ba.writeByte(o.b2);
//			ba.writeByte(o.b1);
//			ba.writeByte(o.b0);
//		
//			ba.writeByte(0x00); // destination device
//			ba.writeByte(int(nodeAddress));
//		
//			ba.writeByte(o.b3); // b values backwards
//			ba.writeByte(o.b2);
//			ba.writeByte(o.b1);
//			ba.writeByte(o.b0);
//		
//			ba.writeByte(0x01); // MSG ID
//			ba.writeByte(0x00);
//		
//			ba.writeByte(0x00); // Flags
//			ba.writeByte(0x00);
//		
//			ba.writeByte(0x00); // How many SV's
//			ba.writeByte(0x01);
//		
//			ba.writeByte(0x00); // ???
//			ba.writeByte(0x00);
//			ba.writeByte(0x01);
//		
//			ba.writeByte(mxInPort); // input #
//			
//			var cs: int = checksum(ba);
//			ba.writeByte(cs);
//			
//			socketSend(ba);
//		}
//		
//		private function sendSetVolume(mxOutPort: int): void {
//			
//			trace("sendSetVolume: " + mxOutPort + " -> " + volumeMap[mxOutPort]);
//			
//			if(!connected)
//				return;
//		
//			var mxInPort: int = routeMap[mxOutPort];
//			var volume: int = volumeMap[mxOutPort];
//		
//			var ba: ByteArray = new ByteArray();
//			ba.writeByte(0x64);
//			ba.writeByte(0x00);
//			ba.writeByte(0x01);
//			
//			ba.writeByte(0x00); // ulong length
//			ba.writeByte(0x00);
//			ba.writeByte(0x00);
//			ba.writeByte(0x1C);
//			
//			ba.writeByte(0x00); // source device
//			ba.writeByte(0x33);
//		
//			var o: Object = zoneCodes[mxOutPort]; 
//			ba.writeByte(o.b3); // b values backwards
//			ba.writeByte(o.b2);
//			ba.writeByte(o.b1);
//			ba.writeByte(o.b0);
//		
//			ba.writeByte(0x00); // destination device
//			ba.writeByte(int(nodeAddress));
//		
//			ba.writeByte(o.b3); // b values backwards
//			ba.writeByte(o.b2);
//			ba.writeByte(o.b1);
//			ba.writeByte(o.b0);
//		
//			ba.writeByte(0x01); // MSG ID
//			ba.writeByte(0x00);
//		
//			ba.writeByte(0x00); // Flags
//			ba.writeByte(0x00);
//		
//			ba.writeByte(0x00); // How many SV's
//			ba.writeByte(0x01);
//			
//			ba.writeByte(0x00); // ???
//			ba.writeByte(0x01);
//			ba.writeByte(0x03);
//		
//			ba.writeShort(volume);
//			
//			var cs: int = checksum(ba);
//			ba.writeByte(cs);
//			
//			socketSend(ba);
//		}
//		
//
	}
}