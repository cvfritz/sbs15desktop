package{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class AudioMatrixSet	extends EventDispatcher {
		
		public static var instance: AudioMatrixSet;

		private var devices: Array = new Array();
		
		public function AudioMatrixSet() {
			instance=this;
		}
		
		public function fromXml(xml: XML): void {
			if(xml==null)
				return;
				
			var xl: XMLList = xml.devices.device;
			for(var i: int=0; i<xl.length(); i++) {
				var device: XML = xl[i];
				var am: AudioMatrix = new AudioMatrix();
				am.fromXml(device);
				devices[i] = am;
				am.addEventListener("mapUpdated", function(event: Event): void {
					this.dispatchEvent(new Event("mapUpdated"));
				});
			}
			this.dispatchEvent(new Event("mapUpdated"));
		}
		
		public function getRoute(zoneId: String): int {
			return getDeviceByZoneId(zoneId).getRoute(getPortByZoneId(zoneId));
		}
		
		public function setRoute(mxPort: int, zoneId: String): void {
			return getDeviceByZoneId(zoneId).setRoute(mxPort, getPortByZoneId(zoneId));
		}
		
		public function getVolume(zoneId: String): int {
			return getDeviceByZoneId(zoneId).getVolume(getPortByZoneId(zoneId));
		}
		
		public function setVolume(zoneId: String, volume: int): void {
			return getDeviceByZoneId(zoneId).setVolume(getPortByZoneId(zoneId), volume);
		}
		
		public function mute(zoneId: String): void {
			return getDeviceByZoneId(zoneId).mute(getPortByZoneId(zoneId));
		}
		
		public function unmute(zoneId: String): void {
			return getDeviceByZoneId(zoneId).unmute(getPortByZoneId(zoneId));
		}

		public function isMuted(zoneId: String): Boolean {
			return getDeviceByZoneId(zoneId).isMuted(getPortByZoneId(zoneId));
		}

		private function getDeviceByZoneId(zoneId: String): AudioMatrix {
			var d: int = int(zoneId.charCodeAt(0)) - int("A".charCodeAt(0));
			return devices[d];
		}
		
		private function getPortByZoneId(zoneId: String): int {
			var d: int = int(zoneId.charAt(2));
			return d;	
		}
		
		public function getRouteMap(): Array {
			var rm: Array = new Array(devices.length);
			for(var i: int=0; i<rm.length; i++) {
				rm[i] = AudioMatrix(devices[i]).getRouteMap();
			}
			return rm;
		}
		
		public function setRouteMap(rm: Array) : void {
			for(var i: int=0; i<rm.length; i++) {
				AudioMatrix(devices[i]).setRouteMap(rm[i]);
			}
		}
		
		public function getClonedRouteMap(): Array {
			var rm: Array = new Array(devices.length);
			for(var i: int=0; i<rm.length; i++) {
				var orig: Array = AudioMatrix(devices[i]).getRouteMap()
				rm[i] = new Array(orig.length);
				for(var j: int=0; j<orig.length; j++)
					rm[i][j] = orig[j];
			}
			return rm;
		}
				
		public function getClonedAudioLevels(): Array {
			var al: Array = new Array(devices.length);
			for(var i: int=0; i<al.length; i++) {
				var orig: Array = AudioMatrix(devices[i]).getVolumeMap()
				al[i] = new Array(orig.length);
				for(var j: int=0; j<orig.length; j++)
					al[i][j] = orig[j];
			}
			return al;
		}
		
		public function getDevice(i: int): AudioMatrix {
			return devices[i];
		}
	}
}