package Devices {
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.Socket;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	
	public class AudioDevice {
		
		[Bindable]
		public var _volumes:ArrayCollection;
		[Bindable]
		public var _mutes:ArrayCollection;
		[Bindable]
		public var _muteStrings:ArrayCollection;
		[Bindable]
		public var _routes:ArrayCollection;
		[Bindable]
		public var _routeTable:ArrayCollection;
		public var _name:String;
		protected var _ID:int;
		public var _socket:Socket;
		public var _outputCount:int;
		
		public function AudioDevice() {
			
		}
		
		public function connect(ip:String, port:int, MAC:String = "", subnet:String = "", gateway:String = ""):void {}
		public function disconnect(e:Event):void {}
		public function setVolume(output:int, value:Number):int {return 0;}
		public function setMute(output:int):int {return 0;}
		public function setRoute(output:int, input:int):int {return 0;}
		public function getName():String {
			return _name; 
		}
		public function getDeviceID():int {
			return _ID;
		}
		public function setDeviceID(id:int):void {
			_ID = id;
		}
		public function subscribe():int {return 0;}
		public function unsubscribe():int {return 0;}
		public function toString():String {
			return _name;
		}
		public function getParameters():void {}
		
		protected function updateRouting(output:int, input:int):void {
			
			try {
				var file2:File = File("C:\\sportsbar\\AudioSourceList.xml");
				//var file2:File = File.applicationStorageDirectory.resolvePath("AudioSourceList.xml");//SourceList is a local file on the iPad	
				var stream2:FileStream = new FileStream();
				stream2.open(file2, FileMode.READ);
				var AudioSourceList:XML = XML(stream2.readUTFBytes(stream2.bytesAvailable));
				var found:Boolean = false;
				for each (var property:XML in AudioSourceList.Source) {
					if (property.devID == _ID && int(property.port) == input) {
						_routes[output] = String(property.Name);
						found = true;
						FlexGlobals.topLevelApplication.addText ("Assigning route to output " + output + ": " + property.Name);
					}
					if (!found) _routes[output] = "";
				}
			} catch (e:IOError) {
				return;	
			}
			finally {
				stream2.close();  
			}
		}
		
		public function testGetParams():void {}
	}
}