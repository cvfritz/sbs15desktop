package Devices {
	import flash.errors.IOError;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	
	public class DBXZonePro extends AudioDevice {
		
		private var _discoBytes:Vector.<uint>;
		private var _inBytes:Vector.<uint>;
		private var _BNumbers:Array;
		private var _SpeakerList:XML;
		
		public function DBXZonePro(name:String, outputs:int, xml:XML) {
			super();
			_socket = new Socket();
			_discoBytes = new Vector.<uint>();
			_name = name;
			_inBytes = new Vector.<uint>();
			
			_volumes = new ArrayCollection();
			_mutes = new ArrayCollection();
			_routes = new ArrayCollection();
			_BNumbers = new Array();
			_muteStrings = new ArrayCollection();
			_outputCount = outputs;
			
			for (var i:int = 0; i < _outputCount; i++) {
				_volumes.addItem(0);
				_mutes.addItem("Mute");
				_routes.addItem("");
				_BNumbers[i] = [-1,-1,-1,-1];
				_muteStrings.addItem("");
			}
			
			_SpeakerList = xml;
		}
		
		
		override public function connect(ip:String, port:int, MAC:String = "", subnet:String = "", gateway:String = ""):void {
			try {_socket.connect(ip, port);}
			catch (e:IOError) {throw e; return;}
			_socket.addEventListener(Event.CONNECT, onConnect);
			_socket.addEventListener(Event.CLOSE, disconnect);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_socket.addEventListener(ProgressEvent.SOCKET_DATA, onResponse);	
			_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecError);
			
			_discoBytes = new Vector.<uint>(); 
			//No ACK, frame start or frame count should be needed
			
			//Header
			_discoBytes.push(0x01);									//Version
			_discoBytes.push(0x00, 0x00, 0x00, 0x17); 				//Packet length from version to end of payload in bytes (23)
			_discoBytes.push(0x00, 0x33, 0x00, 0x00, 0x00, 0x00);	//Source node: IPad node number
			_discoBytes.push(0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00);	//Dest. node: querying = 0xFFFF
			_discoBytes.push(0x00, 0x00);							//Message ID = disco
			_discoBytes.push(0x00, 0x04);							//Flags = Info
			
			//Payload
			_discoBytes.push(0x00, 0x33);							//Payload: source node	
			
		}
		
		private function onError(event:IOErrorEvent):void {
			
			//FlexGlobals.topLevelApplication.addText("Connection error");
			trace ("Connection error");
		}
		
		/**
		 * Connection listener function for socket. Copies the disco bytes into a temporary byte array
		 * and sends the bytes through the socket. onResponse is called when the device responds.
		 * @param event the connection event
		 */
		private function onConnect(event:Event):void {
			//FlexGlobals.topLevelApplication.addText("Socket connected");
			
			var discoByteArray:ByteArray = new ByteArray();
			
			//To establish and maintain connection, we must send a DISCO command asking the device for the node number
			//Load each byte from the disco array into a local byte array
			for each (var byte:uint in _discoBytes) {
				discoByteArray.writeByte(byte);
			}
			
			//Send disco message
			
			_socket.writeBytes(discoByteArray);
			_socket.flush();
			
		}
		
		
		override public function disconnect(event:Event):void {
			_socket.close();
			//FlexGlobals.topLevelApplication.addText ("Closing socket.");
		}
		
		/**
		 * Listener function for data received from the DBX through the TCP socket. This method
		 * reads any bytes that are available and makes sure that it has received as many bytes
		 * as are specified by the length byte (byte 4). It then retrives the node number from 
		 * bytes 21:22 and updates the disco array. 
		 * @param event the data event 
		 */
		private function onResponse(event:ProgressEvent):void {
			
			var inBytes:ByteArray = new ByteArray();		//Temporary byte array to read data into
			_socket.readBytes(inBytes, 0, _socket.bytesAvailable);	//Read available data
			while (inBytes.bytesAvailable > 0) {
				var tempByte:uint = inBytes.readUnsignedByte();
				_inBytes.push(tempByte);	//put data into array
			}
			
			while (_inBytes.length > 4) {
				var packetLength:int = _inBytes[4] + _inBytes[3] * 256 + _inBytes[2] * 65536 + _inBytes[1] * 16777216;
				//FlexGlobals.topLevelApplication.addText("Packet length: " + packetLength + ", _inBytes.length: " + _inBytes.length);
				
				//Messages will have message lengths stored in bytes 1:4. Assume (for now) length < 256.
				if (packetLength <= _inBytes.length) {				//Check if length of packet matches length of array
					var packet:Vector.<uint> = _inBytes.slice(0,packetLength);
					_inBytes = _inBytes.slice(packetLength);
					handlePacket(packet, packetLength);
				}
				else break;
			}
		}
		
		private function handlePacket(packet:Vector.<uint>, packetLength:int):void {
			
			
			///////////SUBSCRIBE MESSAGE/////////////////////////
			if (packet[17] == 0x01 && packet[18] == 0x00) {
				//SUBSCRIBE MESSAGE RECEIVED
				
				//First check which state variable changed (routing, volume, mute)
				
				switch(packet[24]) {
					case 0: 
						updateRouting(packet[9], packet[26]);
						break;
					case 1:
						//Handle volume change. Output number given in byte 9. 
						//New level is in byte 27 (8 LSB)
						var newVolume:Number = ((packet[27] & 0xFF) / 2) - 90.5;
						_volumes[packet[9]] = newVolume;
						break;
					case 2:
						//Handle mute change
						_mutes[packet[9]] = (packet[26] == 0x00) ? "Mute" : "Unmute"; 
						_muteStrings[packet[9]] = (packet[26] == 0x00) ? "" : "Muted"; 
						break;
				}
				
				sendDiscoReply();
			}
				///////////DISCO MESSAGE/////////////////////////	
			else if (packet[17] == 0x00 && packet[18] == 0x00) {		//DISCO MESSAGE RECEIVED
				
				var nodeNumber:uint = packet[5] * 256 + packet[6];		//Convert 2 bytes into one number
				_discoBytes[11] = packet[5];									//Update disco message for next send
				_discoBytes[12] = packet[6];
				_ID = packet[5] * 256 + packet[6];
				packet = new Vector.<uint>();											//Empty byte array
				//TODO: do something with nodeNumber (put in XML, display, etc.)
				//FlexGlobals.topLevelApplication.addText("marco! " + nodeNumber);
				
				sendDiscoReply();
				
			}
				
			else if (packet[17] == 0x01 && packet[18] == 0x03) {
				for (var i:int = 0; i < _outputCount; i++) {
					if (_BNumbers[i][3] == packet[7] && _BNumbers[i][2] == packet[8] &&
						_BNumbers[i][1] == packet[9] && _BNumbers[i][0] == packet[10]) {
						//FlexGlobals.topLevelApplication.addText("Updating parameters for Output " + i);
						updateRouting(i,packet[26]);
						_volumes[i] = ((packet[31] & 0xFF) / 2) - 90.5;
						_mutes[i] = (packet[35] == 0x00) ? "Mute" : "Unmute";
						_muteStrings[i] = (packet[35] == 0x00) ? "" : "Muted";
					}	
				}
			}
		}
		
		
		private function onSecError(event:Event):void {
			//FlexGlobals.topLevelApplication.addText ("Security error");
		}
		
		private function sendDiscoReply():void {
			var discoByteArray:ByteArray = new ByteArray();
			//To maintain connection, we must send a DISCO command asking the device for the node number
			//Load each byte from the disco array (with device node number) into a local byte array
			for each (var byte:uint in _discoBytes) {
				discoByteArray.writeByte(byte);
			}
			//Send disco message
			_socket.writeBytes(discoByteArray);
			_socket.flush();
			//FlexGlobals.topLevelApplication.addText("polo!");
		}
		
		/**
		 * Test function to change the gain to max. 
		 * Need to confirm: order of Object ID "b-numbers" (big-endian)
		 * 
		 */
		override public function setVolume(output:int, value:Number):int {
			var status:int = getBNumbers();
			if (!status) {
				var testVolumeArray:Array = new Array();
				_volumes[output] = value;
				var scaledVolume:int = Math.round((value + 90) * (221/110));
				
				//Header
				testVolumeArray.push(0x01);					//Version #
				testVolumeArray.push(0x00, 0x00, 0x00, 0x1C);//Length of packet
				testVolumeArray.push(0x00, 0x33);			//Local device address
				testVolumeArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]);
				testVolumeArray.push(_ID / 256, _ID % 256);		//Received node number
				testVolumeArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]);
				testVolumeArray.push(0x01, 0x00);				//Message id for SET command
				testVolumeArray.push(0x00, 0x00);				//Flags -- none set
				
				//Payload
				testVolumeArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testVolumeArray.push(0x00, 0x01);				//State variable ID in the object 
				testVolumeArray.push(0x03);						//Data type = UWORD (16 bits, unsigned)
				testVolumeArray.push(scaledVolume / 256, scaledVolume % 256);	 //New volume value, high and low bytes
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				for each (var byte:uint in testVolumeArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);
					
				}
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();	
				//flush socket
				
			}
			return status;
			
		}
		
		override public function setMute(output:int):int {
			var status:int = getBNumbers();
			//FlexGlobals.topLevelApplication.addText("BNumber status is " + status);
			if (!status) {
				var testMuteArray:Array = new Array();
				var mute:Boolean = _mutes[output] == "Mute";
				_mutes[output-1] = mute ? "Unmute" : "Mute";
				_muteStrings[output-1] = mute ? "Muted" : "";
				
				//Header
				testMuteArray.push(0x01);					//Version #
				testMuteArray.push(0x00, 0x00, 0x00, 0x1B);//Length of packet
				testMuteArray.push(0x00, 0x33);			//Local device address
				testMuteArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]);
				testMuteArray.push(_ID / 256, _ID % 256);		//Received node number
				testMuteArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]);
				testMuteArray.push(0x01, 0x00);				//Message id for SET command
				testMuteArray.push(0x00, 0x00);				//Flags -- none set
				
				//Payload
				testMuteArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testMuteArray.push(0x00, 0x02);				//State variable ID in the object 
				testMuteArray.push(0x01);						//Data type = UBYTE (8 bits, unsigned)
				testMuteArray.push((mute ? 0x01 : 0x00));	 //New mute value, 1 = mute, 0 = unmute
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				for each (var byte:uint in testMuteArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);
					
				}
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();								//flush socket 
			}
			return status;
			
			
		}
		
		
		override public function setRoute(output:int, input:int):int {
			var status:int = getBNumbers();
			if (!status) {
				updateRouting(output, input);
				var testRouteArray:Array = new Array();
				
				//Header
				testRouteArray.push(0x01);					//Version #
				testRouteArray.push(0x00, 0x00, 0x00, 0x1B);//Length of packet
				testRouteArray.push(0x00, 0x33);			//Local device address
				testRouteArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]); //BNumbers
				testRouteArray.push(_ID / 256, _ID % 256);		//Received node number
				testRouteArray.push(_BNumbers[output][3],_BNumbers[output][2],_BNumbers[output][1],_BNumbers[output][0]); //BNumbers
				testRouteArray.push(0x01, 0x00);				//Message id for SET command
				testRouteArray.push(0x00, 0x00);				//Flags -- none set
				
				//Payload
				testRouteArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testRouteArray.push(0x00, 0x00);				//State variable ID in the object 
				testRouteArray.push(0x01);						//Data type = UBYTE (8 bits, unsigned)
				testRouteArray.push(input);	 					//New routing value.
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				for each (var byte:uint in testRouteArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);
					
				}
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();								//flush socket
			}
			return status;
			
			
		}
		
		override public function subscribe():int {
			var status:int = getBNumbers();
			if (!status) {
				var i:int = 0;
				for (i = 0; i < _outputCount; i++) { 							//Subscribe to all outputs
					
					var testSubscribeArray:Array = new Array();
					
					//Header
					testSubscribeArray.push(0x01);					//Version #
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x20);//Length of packet
					testSubscribeArray.push(0x00, 0x33);			//Local device address
					testSubscribeArray.push(_BNumbers[i][3],_BNumbers[i][2],_BNumbers[i][1],_BNumbers[i][0]); //BNumbers
					testSubscribeArray.push(_ID / 256, _ID % 256);		//Received node number
					testSubscribeArray.push(_BNumbers[i][3],_BNumbers[i][2],_BNumbers[i][1],_BNumbers[i][0]); //BNumbers
					testSubscribeArray.push(0x01, 0x13);				//Message id for SUBSCRIBE command
					testSubscribeArray.push(0x05, 0x00);				//Flags -- none set
					
					//Payload
					testSubscribeArray.push(0x00, 0x33);				//Local device address
					testSubscribeArray.push(0x01, 0x05, 0x00 + i, 0x20 + i); //Object ID (b3, b2, b1, b0)
					testSubscribeArray.push(0x01);						//Sensor off
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x01);	//Sensor length in ms
					
					var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
					for each (var byte:uint in testSubscribeArray) {	//Copy each byte into temporary send byte array
						tempByteArray.writeByte(byte);
						
					}
					_socket.writeBytes(tempByteArray);				//Send the bytes over
					_socket.flush();								//flush socket
				}
			}
			return status;
			
		}
		
		override public function unsubscribe():int {
			var status:int = getBNumbers();
			if (!status) {
				
				var i:int = 0;
				for (i = 0; i < _outputCount; i++) { 							//Unsubscribe from all outputs
					
					var testSubscribeArray:Array = new Array();
					
					//Header
					testSubscribeArray.push(0x01);					//Version #
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x20);//Length of packet
					testSubscribeArray.push(0x00, 0x33);			//Local device address
					testSubscribeArray.push(_BNumbers[i][3],_BNumbers[i][2],_BNumbers[i][1],_BNumbers[i][0]); //BNumbers
					testSubscribeArray.push(_ID / 256, _ID % 256);		//Received node number
					testSubscribeArray.push(_BNumbers[i][3],_BNumbers[i][2],_BNumbers[i][1],_BNumbers[i][0]); //BNumbers
					testSubscribeArray.push(0x01, 0x14);				//Message id for SUBSCRIBE command
					testSubscribeArray.push(0x00, 0x00);				//Flags -- none set
					
					//Payload
					testSubscribeArray.push(0x00, 0x33);				//Local device address
					testSubscribeArray.push(0x01, 0x05, 0x00 + i, 0x20 + i); //Object ID (b3, b2, b1, b0)
					testSubscribeArray.push(0x01);						//Sensor off
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x01);	//Sensor length in ms
					
					var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
					for each (var byte:uint in testSubscribeArray) {	//Copy each byte into temporary send byte array
						tempByteArray.writeByte(byte);
						
					}
					_socket.writeBytes(tempByteArray);				//Send the bytes over
					_socket.flush();								//flush socket
				}
			}
			return status;
			
		}
		
		override public function getParameters():void {
			
			var testGetArray:Array = new Array();
			
			testGetArray.push(0x01);					//Version #
			testGetArray.push(0x00, 0x00, 0x00, 0x15);//Length of packet
			testGetArray.push(0x00, 0x33);			//Local device address
			testGetArray.push(0x01, 0x00, 0x00, 0x00); //Object ID (b3, b2, b1, b0) given by Andy
			testGetArray.push(_ID / 256, _ID % 256);		//Received node number
			testGetArray.push(0x01, 0x00, 0x00, 0x00); //Object ID (b3, b2, b1, b0) given by Andy
			testGetArray.push(0x01, 0x19);				//Message id for GET command
			testGetArray.push(0x00, 0x01);				//Flags -- Information
			
			
			var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
			for each (var byte:uint in testGetArray) {	//Copy each byte into temporary send byte array
				tempByteArray.writeByte(byte);
				
			}
			_socket.writeBytes(tempByteArray);				//Send the bytes over
			_socket.flush();								//flush socket
			
		}
		
		private function getBNumbers():int {
			
			try {
				
				
				for each (var zone:XML in _SpeakerList.zones.zone) {
					var id:String = zone.id;
					var zoneNum:int = int(id.substr(2));
					_BNumbers[zoneNum][0] = int(zone.b0);
					_BNumbers[zoneNum][1] = int(zone.b1);
					_BNumbers[zoneNum][2] = int(zone.b2);
					_BNumbers[zoneNum][3] = int(zone.b3);
					
					
				}
				for (var i:int = 0; i < _outputCount; i++) 
					for (var j:int = 0; j < 4; j++) if (_BNumbers[i][j] == -1) return 1;	//BNumber not specified error
				return 0;
				
			} catch (e:Error) {				//Null pointer exception or file error
				return 2;	
			}	
			return 0;
		}
		
		
	}
}