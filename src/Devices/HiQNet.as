package Devices {
	import flash.errors.IOError;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	
	import org.osmf.events.TimeEvent;
	
	public class HiQNet extends AudioDevice {
		
		private var _discoBytes:Vector.<uint>;
		private var _inBytes:Vector.<uint>;
		private var _BNumbers:Array;
		private var _SpeakerList:XML;
		private var _timer:Timer = new Timer(4000);
		
		public function HiQNet(name:String, outputs:int, xml:XML) {
			super();
			_timer.addEventListener(TimerEvent.TIMER, function (e:TimerEvent):void {
				sendDiscoReply();
			});
			_socket = new Socket();
			_discoBytes = new Vector.<uint>();
			_name = name;
			_inBytes = new Vector.<uint>();
			
			_volumes = new ArrayCollection();
			_mutes = new ArrayCollection();
			_routes = new ArrayCollection();
			_BNumbers = new Array();
			_outputCount = outputs;
			
			for (var i:int = 0; i < _outputCount; i++) {
				_volumes.addItem(0);
				_mutes.addItem("Mute");
				_routes.addItem("");
				_BNumbers[i] = [-1,-1,-1,-1];
			}
			
			_SpeakerList = xml;
		}
		
		override public function connect(ip:String, port:int, MAC:String = "", subnet:String = "", gateway:String = ""):void {
			
			//catch (e:IOError) {throw e; return;}
			var macArray:Array = new Array(); 
			for each (var i:String in MAC.split(":")) macArray.push(parseInt(i,16));
			var ipArray:Array = new Array();
			for each (var i:String in ip.split(".")) ipArray.push(parseInt(i,16));
			var subnetArray:Array = new Array();
			for each (var i:String in subnet.split(".")) subnetArray.push(parseInt(i,16));
			var gatewayArray:Array = new Array();
			for each (var i:String in gateway.split(".")) gatewayArray.push(parseInt(i,16));
			
			_socket.addEventListener(Event.CONNECT, onConnect);
			_socket.addEventListener(Event.CLOSE, disconnect);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_socket.addEventListener(ProgressEvent.SOCKET_DATA, onResponse);	
			_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecError);
			FlexGlobals.topLevelApplication.addText("Connecting to " + ip + ":" + port);
			_socket.connect(ip, port);
			_discoBytes = new Vector.<uint>(); 
			//No ACK, frame start or frame count should be needed
			
			//Header
			_discoBytes.push(0x02);									//Version
			_discoBytes.push(0x19);									//Header length in bytes
			_discoBytes.push(0x00, 0x00, 0x00, 0x48); 				//Packet length from version to end of payload in bytes (23)
			_discoBytes.push(0x00, 0x33, 0x00, 0x00, 0x00, 0x00);	//Source node: IPad node number
			_discoBytes.push(0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00);	//Dest. node: querying = 0xFFFF
			_discoBytes.push(0x00, 0x00);							//Message ID = disco
			_discoBytes.push(0x00, 0x20);							//Flags = Info
			_discoBytes.push(0x05);									//Hop count - default
			_discoBytes.push(0x00, 0x00);							//Sequence number = 0
			
			//Payload
			_discoBytes.push(0x00, 0x33);							//Payload: source node	
			_discoBytes.push(0x01);									//Cost
			_discoBytes.push(0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0xFD, 0x01, 0x02, 0x03, 0x04);	//Sender Serial # 
			_discoBytes.push(0x00,0x00,0x27,0x10);					//Keep alive = 10000ms
			_discoBytes.push(0x4E,0x20);							//Max message size
			_discoBytes.push(0x01);									//NetworkID = TCP/IP
			_discoBytes.push(macArray[0],macArray[1],macArray[2],
				macArray[3],macArray[4],macArray[5]);				//Sender MAC address
			_discoBytes.push(0x01);									//DHCP
			_discoBytes.push(ipArray[0], ipArray[1], ipArray[2], ipArray[3]);	//IP address
			_discoBytes.push(subnetArray[0],subnetArray[1],subnetArray[2],subnetArray[3]); //Subnet mask
			_discoBytes.push(gatewayArray[0],gatewayArray[1],gatewayArray[2],gatewayArray[3]); //Gateway
			
		}
		
		private function onError(event:IOErrorEvent):void {
			
			FlexGlobals.topLevelApplication.addText("Connection error");
		}
		
		/**
		 * Connection listener function for socket. Copies the disco bytes into a temporary byte array
		 * and sends the bytes through the socket. onResponse is called when the device responds.
		 * @param event the connection event
		 */
		private function onConnect(event:Event):void {
			FlexGlobals.topLevelApplication.addText("Socket connected");
			
			var discoByteArray:ByteArray = new ByteArray();
			
			//To establish and maintain connection, we must send a DISCO command asking the device for the node number
			//Load each byte from the disco array into a local byte array
			for each (var byte:uint in _discoBytes) {
				discoByteArray.writeByte(byte);
			}
			
			//Send disco message
			
			_socket.writeBytes(discoByteArray);
			_socket.flush();
			getParameters();
			
		}
		
		
		override public function disconnect(e:Event):void {
			FlexGlobals.topLevelApplication.addText("Socket closed");
			_socket.close();
		}
		
		/**
		 * Listener function for data received from the DBX through the TCP socket. This method
		 * reads any bytes that are available and makes sure that it has received as many bytes
		 * as are specified by the length byte (byte 4). It then retrives the node number from 
		 * bytes 21:22 and updates the disco array. 
		 * @param event the data event 
		 */
		private function onResponse(event:ProgressEvent):void {
			_inBytes = new Vector.<uint>();
			var inBytes:ByteArray = new ByteArray();		//Temporary byte array to read data into
			_socket.readBytes(inBytes, 0, _socket.bytesAvailable);	//Read available data
			
			for (var i:int = 0; i < inBytes.length; i++) {
				if (inBytes[i] == 0x02 && inBytes[i+1] == 0x19 && inBytes[i+2] == 0x00 &&
					inBytes[i+3] == 0x00 && inBytes[i+4] == 0x00 && _inBytes.length > 0) {
					//We have a second packet here - need to parse what's there first
					parseInput();
					_inBytes = new Vector.<uint>();
				}
				_inBytes.push(inBytes[i]);
				
			}
			FlexGlobals.topLevelApplication.addText2("1Received bytes: " + _inBytes.length);
			parseInput();
			
			FlexGlobals.topLevelApplication.addText2("2Received bytes: " + _inBytes.length);
			
			
		}
		
		private function parseInput():void {
			//Messages will have message lengths stored in bytes 1:4. Assume (for now) length < 256.
			if (_inBytes[5] == _inBytes.length) {				//Check if length of packet matches length of array
				
				
				///////////SUBSCRIBE MESSAGE/////////////////////////
				if (_inBytes[18] == 0x01 && _inBytes[19] == 0x00) {
					//SUBSCRIBE MESSAGE RECEIVED
					FlexGlobals.topLevelApplication.addText2 ("subscribe received");
					//First check which state variable changed (routing, volume, mute)
					
					switch(_inBytes[24]) {
						case 0: 
							updateRouting(_inBytes[9], _inBytes[26]); //this was commented out
							break;
						case 1:
							//Handle volume change. Output number given in byt 9. 
							//New level is in byte 27 (8 LSB)
							var newVolume:Number = ((_inBytes[27] & 0xFF) / 2) - 90;
							_volumes[_inBytes[9]] = newVolume;
							FlexGlobals.topLevelApplication.addText2(_inBytes[9] + "volume " + newVolume);
							break;
						case 2:
							//Handle mute change
							_mutes[_inBytes[9]] = (_inBytes[26] == 0x01) ? "Mute" : "Unmute"; 
							break;
					}
					
					
					
				}
					
					///////////DISCO MESSAGE/////////////////////////	
				else if (_inBytes[18] == 0x00 && _inBytes[19] == 0x00) {		//DISCO MESSAGE RECEIVED
					if (_discoBytes[12] == 0xFF) {
						var nodeNumber:uint = _inBytes[6] * 256 + _inBytes[7];		//Convert 2 bytes into one number
						_discoBytes[12] = _inBytes[6];									//Update disco message for next send
						_discoBytes[13] = _inBytes[7];
						_ID = _inBytes[6] * 256 + _inBytes[7];
						//Empty byte array
						//TODO: do something with nodeNumber (put in XML, display, etc.)
						FlexGlobals.topLevelApplication.addText("marco! " + nodeNumber);
						_timer.start();
						sendDiscoReply();
					}
					_inBytes = new Vector.<uint>();	
				}
					////////////PARAM GET MESSAGE////////////////////////
				else if (_inBytes[18] == 0x01 && _inBytes[19] == 0x03) {		//PARAM GET MESSAGE RECEIVED
					FlexGlobals.topLevelApplication.addText2 ("Param Get received");
					var output:int = -1;
					//First, find which output this message is for by checking B numbers for equality
					for (var i:int = 0; i < _outputCount; i++) 
						if (_BNumbers[i][0] == _inBytes[8] && _BNumbers[i][1] == _inBytes[9] &&
							_BNumbers[i][2] == _inBytes[10] && _BNumbers[i][3] == _inBytes[11])
							output = i;
					FlexGlobals.topLevelApplication.addText2("Array values from 30:39: ");
					FlexGlobals.topLevelApplication.addText2("_inBytes Length = " + _inBytes.length);
					//for (var i:int = 0; i < 10; i++) FlexGlobals.topLevelApplication.addText("" + _inBytes[30+i] + " ");
					//updateRouting(output, _inBytes[30], _inBytes[31]);				//New routing value
					var next1:int = 0;
					const start:int = 29;
					var volBytes:ByteArray = new ByteArray();
					for (var i:int = 0; i < 3; i++) {
						switch (_inBytes[start+next1]) {
							case 0x01:					//This is a mute
								_mutes[output] = (_inBytes[start+next1+1] == 0x00) ? "Mute" : "Unmute"; //New mute value
								next1 += 4;
								FlexGlobals.topLevelApplication.addText("Setting mute: output " + output + ", " + _inBytes[42]);
								break;
							case 0x03:					//This is a route
								updateRouting(output, _inBytes[start+next1+2]);		//ASSUMING THAT INPUT < 256
								next1 += 5;
								break;
							case 0x06:					//This is a volume
								volBytes.writeByte(_inBytes[start+next1+1]); volBytes.writeByte(_inBytes[start+next1+2]); 
								volBytes.writeByte(_inBytes[start+next1+3]); volBytes.writeByte(_inBytes[start+next1+4]);
								volBytes.position = 0;
								var newVol:Number = volBytes.readFloat();
								_volumes[output] = newVol;				
								next1 += 7;
								FlexGlobals.topLevelApplication.addText2("Setting volume: output " + output + ", " + newVol);
								break;	
						}
					}
					
				}
			}
		}
		
		
		private function onSecError(event:Event):void {
			FlexGlobals.topLevelApplication.addText ("Security error");
		}
		
		private function sendDiscoReply():void {
			var discoByteArray:ByteArray = new ByteArray();
			//To maintain connection, we must send a DISCO command asking the device for the node number
			//Load each byte from the disco array (with device node number) into a local byte array
			for each (var byte:uint in _discoBytes) {
				discoByteArray.writeByte(byte);
			}
			//Send disco message
			_socket.writeBytes(discoByteArray);
			_socket.flush();
			FlexGlobals.topLevelApplication.addText("polo!");
		}
		
		
		override public function setVolume(output:int, value:Number):int {
			var status:int = getBNumbers();
			if (!status) {
				var testVolumeArray:Array = new Array();
				_volumes[output] = value;
				//Converting IEEE754 number to hex array
				var positive:Boolean = value >= 0;
				
				var scaledVolume:int = Math.round((value + 90) * (221/110));
				var convert:int = int(value);
				
				//Header
				testVolumeArray.push(0x02);					//Version #
				testVolumeArray.push(0x19);					//Header length
				testVolumeArray.push(0x00, 0x00, 0x00, 0x22);//Length of packet
				testVolumeArray.push(0x00, 0x33);			//Local device address
				testVolumeArray.push(0x00, 0x00, 0x00, 0x00);
				testVolumeArray.push(_ID / 256, _ID % 256);		//Received node number
				testVolumeArray.push(_BNumbers[output][0],_BNumbers[output][1],_BNumbers[output][2],_BNumbers[output][3]);
				testVolumeArray.push(0x01, 0x00);				//Message id for SET command
				testVolumeArray.push(0x00, 0x20);				//Flags -- none set
				testVolumeArray.push(0x05);							//Hop count - default
				testVolumeArray.push(0x00, 0x00);					//Sequence number = 0
				
				//Payload
				testVolumeArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testVolumeArray.push(0x00, 0x01);				//State variable ID in the object 
				testVolumeArray.push(0x06);						//Data type = UWORD (16 bits, unsigned)
				//testVolumeArray.push(scaledVolume / 256, scaledVolume % 256);	 //New volume value, high and low bytes
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				
				for each (var byte:uint in testVolumeArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);	
				}
				tempByteArray.writeFloat(value);
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();	
				//flush socket
				
			}
			return status;
			
		}
		
		override public function setMute(output:int):int {
			var status:int = getBNumbers();
			FlexGlobals.topLevelApplication.addText("BNumber status is " + status);
			if (!status) {
				var testMuteArray:Array = new Array();
				var mute:Boolean = _mutes[output] == "Mute";
				_mutes[output] = mute ? "Unmute" : "Mute";
				
				//Header
				testMuteArray.push(0x02);					//Version #
				testMuteArray.push(0x19);					//Header length
				testMuteArray.push(0x00, 0x00, 0x00, 0x1F);//Length of packet
				testMuteArray.push(0x00, 0x33);			//Local device address
				testMuteArray.push(0x00,0x00,0x00,0x00);
				testMuteArray.push(_ID / 256, _ID % 256);		//Received node number
				testMuteArray.push(_BNumbers[output][0],_BNumbers[output][1],_BNumbers[output][2],_BNumbers[output][3]);
				testMuteArray.push(0x01, 0x00);				//Message id for SET command
				testMuteArray.push(0x00, 0x20);				//Flags -- none set
				testMuteArray.push(0x05);							//Hop count - default
				testMuteArray.push(0x00, 0x00);					//Sequence number = 0
				
				//Payload
				testMuteArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testMuteArray.push(0x00, 0x02);				//State variable ID in the object 
				testMuteArray.push(0x01);						//Data type = UWORD (16 bits, unsigned)
				//testMuteArray.push((mute ? 0x01 : 0x00));	 //New mute value, 1 = mute, 0 = unmute
				testMuteArray.push(0x00);
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				for each (var byte:uint in testMuteArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);
					
				}
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();								//flush socket 
			}
			return status;
			
			
		}
		
		
		override public function setRoute(output:int, input:int):int {
			var status:int = getBNumbers();
			if (!status) {
				//updateRouting(output-1, input);
				var testRouteArray:Array = new Array();
				
				//Header
				testRouteArray.push(0x02);					//Version #
				testRouteArray.push(0x19);					//Header length
				testRouteArray.push(0x00, 0x00, 0x00, 0x20);//Length of packet
				testRouteArray.push(0x00, 0x33);			//Local device address
				testRouteArray.push(0x00,0x00,0x00,0x00);
				testRouteArray.push(_ID / 256, _ID % 256);		//Received node number
				testRouteArray.push(_BNumbers[output][0],_BNumbers[output][1],_BNumbers[output][2],_BNumbers[output][3]);
				testRouteArray.push(0x01, 0x00);			//Message id for SET command
				testRouteArray.push(0x00, 0x20);				//Flags -- none set
				testRouteArray.push(0x05);							//Hop count - default
				testRouteArray.push(0x00, 0x00);					//Sequence number = 0
				
				//Payload
				testRouteArray.push(0x00, 0x01);				//Changing 1 State Var in this frame
				testRouteArray.push(0x00, 0x00);				//State variable ID in the object 
				testRouteArray.push(0x03);						//Data type = UWORD (16 bits, unsigned)
				//testRouteArray.push(0x00);						//Assume < 256 input sources
				testRouteArray.push(input / 256, input % 256);	 					//New routing value.
				
				
				var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
				for each (var byte:uint in testRouteArray) {	//Copy each byte into temporary send byte array
					tempByteArray.writeByte(byte);
					
				}
				_socket.writeBytes(tempByteArray);				//Send the bytes over
				_socket.flush();								//flush socket
				FlexGlobals.topLevelApplication.addText2("Route Set");
			}
			return status;
			
			
		}
		
		override public function subscribe():int {
			var status:int = getBNumbers();
			if (!status) {
				var i:int = 0;
				for (i = 0; i < _outputCount; i++) { 							//Subscribe to all outputs
					
					var testSubscribeArray:Array = new Array();
					//Header
					testSubscribeArray.push(0x02);					//Version #
					testSubscribeArray.push(0x19);					//Header length
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x24);//Length of packet
					testSubscribeArray.push(0x00, 0x33);			//Local device address
					testSubscribeArray.push(_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]); //B#'s
					testSubscribeArray.push(_ID / 256, _ID % 256);		//Received node number
					testSubscribeArray.push(_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]); //B#'s
					testSubscribeArray.push(0x01, 0x13);				//Message id for SUBSCRIBE command
					testSubscribeArray.push(0x02, 0x20);				//Flags -- none set
					testSubscribeArray.push(0x05);							//Hop count - default
					testSubscribeArray.push(0x00, 0x00);					//Sequence number = 0
					//Payload
					testSubscribeArray.push(0x00, 0x33);			//Local address
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x00); //B#'s
					testSubscribeArray.push(0x01);					//Sub type = NON SENSOR
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x01);	//Sensor rate placeholder
					
					var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
					for each (var byte:uint in testSubscribeArray) {	//Copy each byte into temporary send byte array
						tempByteArray.writeByte(byte);
						
					}
					_socket.writeBytes(tempByteArray);				//Send the bytes over
					_socket.flush();								//flush socket
				}
			}
			return status;
			
		}
		
		override public function unsubscribe():int {
			var status:int = getBNumbers();
			if (!status) {
				
				var i:int = 0;
				for (i = 0; i < _outputCount; i++) { 							//Unsubscribe from all outputs
					
					var testSubscribeArray:Array = new Array();
					
					//Header
					testSubscribeArray.push(0x02);					//Version #
					testSubscribeArray.push(0x19);					//Header length
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x20);//Length of packet
					testSubscribeArray.push(0x00, 0x33);			//Local device address
					testSubscribeArray.push(_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]); //BNumbers
					testSubscribeArray.push(_ID / 256, _ID % 256);		//Received node number
					testSubscribeArray.push(_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]); //BNumbers
					testSubscribeArray.push(0x01, 0x14);				//Message id for UNSUBSCRIBE command
					testSubscribeArray.push(0x02, 0x20);				//Flags -- none set
					testSubscribeArray.push(0x05);						//Default hop count
					testSubscribeArray.push(0x00,0x00);					//Default sequence number
					
					//Payload
					testSubscribeArray.push(0x00, 0x33);				//Local device address
					testSubscribeArray.push(0x00, 0x00, 0x00, 0x00); 	//Object ID (b3, b2, b1, b0)
					testSubscribeArray.push(0x01);						//No sensor
					
					var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
					for each (var byte:uint in testSubscribeArray) {	//Copy each byte into temporary send byte array
						tempByteArray.writeByte(byte);
						
					}
					_socket.writeBytes(tempByteArray);				//Send the bytes over
					_socket.flush();								//flush socket
				}
			}
			return status;
			
		}
		
		override public function getParameters():void {
			var status:int = getBNumbers();
			if (!status) {
				for (var i:int = 0; i < _outputCount; i++) {
					var getMessageArray:Array = new Array();
					
					//Header
					getMessageArray.push(0x02);							//Protocol version
					getMessageArray.push(0x19);							//Header length
					getMessageArray.push(0x00,0x00,0x00,0x21);			//Message length
					//Node:BNumbers for source and destination
					getMessageArray.push(0x00,0x33,_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]);
					getMessageArray.push(_ID/256,_ID%256,_BNumbers[i][0],_BNumbers[i][1],_BNumbers[i][2],_BNumbers[i][3]);	
					getMessageArray.push(0x01,0x03);					//Message ID for get message
					getMessageArray.push(0x00,0x00);					//Flags -- none set
					getMessageArray.push(0x05);							//Hop count = default
					getMessageArray.push(0x00,0x00);					//Sequence number = 0
					
					//Payload
					getMessageArray.push(0x00,0x03);					//Requesting 3 parameters
					getMessageArray.push(0x00,0x00);					//Get routing data
					getMessageArray.push(0x00,0x01);					//Get level data
					getMessageArray.push(0x00,0x02);					//Get mute data
					
					var tempByteArray:ByteArray = new ByteArray();	//Byte array for sending
					for each (var byte:uint in getMessageArray) {	//Copy each byte into temporary send byte array
						tempByteArray.writeByte(byte);
						
					}
					_socket.writeBytes(tempByteArray);				//Send the bytes over
					_socket.flush();								//flush socket
				}
			}
			
			
		}
		
		private function getBNumbers():int {
			
			try {
				
				
				for each (var zone:XML in _SpeakerList.zones.zone) {
					var id:String = zone.id;
					var zoneNum:int = int(id.substr(2));
					_BNumbers[zoneNum][0] = int(zone.b0);
					_BNumbers[zoneNum][1] = int(zone.b1);
					_BNumbers[zoneNum][2] = int(zone.b2);
					_BNumbers[zoneNum][3] = int(zone.b3);
					
					
				}
				for (var i:int = 0; i < _outputCount; i++) 
					for (var j:int = 0; j < 4; j++) if (_BNumbers[i][j] == -1) return 1;	//BNumber not specified error
				return 0;
				
			} catch (e:Error) {				//Null pointer exception or file error
				return 2;	
			}	
			return 0;
		}
		
	}
}