package {
	public class SchedulerEvent {
		
		private static var dayNames: Array = new Array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
		
		[Bindable]
		public var type: int=1;
		
		[Bindable]
		public var hours: int=1;
		
		[Bindable]
		public var minutes: int=0;
		
		[Bindable]
		public var amOrPm: String="am";
		
		[Bindable]
		public var day: int=1;
		
		[Bindable]
		public var month: int=1;
		
		[Bindable]
		public var year: int=2010;
		
		[Bindable]
		public var channelNumber: int=0;
		
		[Bindable]
		public var description: String="";
		
		[Bindable]
		public var dueSeconds: Number=0;
		
		[Bindable]
		public var dueStr: String="";

		[Bindable]
		public var days: String="";

		[Bindable]
		public var fullDate: String="";
		
		[Bindable]
		public var fullTime: String="";
		
		[Bindable]
		public var satBox: VideoSourceItem=null;
		
		[Bindable]
		public var satBoxName: String="";

		[Bindable]
		public var toBeDeleted: Boolean=false;

		[Bindable]
		public var executionFlag: Boolean=false;

		[Bindable]
		public var visible: Boolean=true;

		public function SchedulerEvent(o: Object=null) {
			if(o!=null) {
				type=o.type;
				hours=o.hours;
				minutes=o.minutes;
				amOrPm=o.amOrPm;
				day=o.day;
				month=o.month;
				year=o.year;
				channelNumber=o.channelNumber;
				description=o.description;
				days=o.days;
				fullDate=o.fullDate;
				fullTime=o.fullTime;
				satBoxName=o.satBoxName;
			} else {
				var now: Date = new Date();
				day=now.getDate();
				month=now.getMonth();
				year=now.getFullYear();
				fullDate = ""+now;
			}
		}
		
		public function calculate(): void {
			
			if(toBeDeleted)
				return;
			
			fullTime = hours+":"+minutes+" "+amOrPm;
			fullDate = (month+1)+"/"+day+"/"+year;
			
			description="";
			switch(type) {
				case 1:
					description = "Once, on " + fullDate + " at " + fullTime;
					var now: Date = new Date();
					var then: Date = new Date(year, month, day, hours + (amOrPm=="pm"?12:0), minutes);
					dueSeconds = int((then.getTime() - now.getTime())/1000);
					if(dueSeconds<0) {
						if(satBox!=null)
							satBox.setChannel(channelNumber);
						toBeDeleted=true;
					}						
					break;
				case 2:
					description = "Every day at " + fullTime;
					var now: Date = new Date();
					var then: Date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), hours + (amOrPm=="pm"?12:0), minutes);
					dueSeconds = int((then.getTime() - now.getTime())/1000);
					if(dueSeconds<-10) {
						dueSeconds = 24*60*60 + dueSeconds;
						executionFlag=false;
					} else if(dueSeconds<0 && executionFlag==false) {
						satBox.setChannel(channelNumber);
						executionFlag=true;
					}
					break;
				case 3:
					for(var i: int=0; i<days.length; i++) {
						if(days.charAt(i)=="y")
							description += dayNames[i] + ", " ;
					}
					if(description.length==0)
						description = "No days, ";
					description = description.substr(0, description.length-2);
					description += " at " + fullTime;
					var now: Date = new Date();
					var today: int = now.getDay()==0?6:now.getDay();
					var day: int = today;
					dueSeconds=1000000;
					var then: Date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), hours + (amOrPm=="pm"?12:0), minutes);
					for(var i:int=0; i<7; i++) {
						if(days.charAt(day-1)=="y") {
							dueSeconds = (then.getTime() - now.getTime())/1000;
							if(dueSeconds>0)
								break;
						}
						then = new Date(then.getTime() + 24*60*60*1000); 
						day=(day+1)%7;
					}
					if(dueSeconds>20) {
						executionFlag=false;
					}
					if(dueSeconds<10 && executionFlag==false) {
						satBox.setChannel(channelNumber);
						executionFlag=true;
					}
					break;
			}
			
			dueSeconds = Math.round(dueSeconds);
			{
				var t: Number = dueSeconds;
				var d: Number = Math.floor(t / (24*60*60));
				t -= d * 24*60*60;
				var h: Number = Math.floor(t / (60*60));
				t -= h * 60*60;
				var m: Number = Math.floor(t / 60);
				t -= m * 60;
				var s: Number = 0;
				if(h==0)
					s=Math.floor(t);
				dueStr = (d!=0?""+d+"d ":"") + (h!=0?""+h+"h ":"") + (m!=0?""+m+"m ":"") + (s!=0?""+s+"s ":"");
			}
		}
	}
}
