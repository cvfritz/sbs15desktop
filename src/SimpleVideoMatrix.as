package {
	import flash.events.*;
	import flash.net.SharedObject;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Timer;

	public class SimpleVideoMatrix {
		
		public static var instance: SimpleVideoMatrix;
				
		public var connected: Boolean=false;
		public var address: String="";
		public var port: int=0;
		public var useMeForComputerVideo: Boolean=false;
		
		private var socket: Socket = new Socket();
		private var currentOutPort: int =0;
		
		public function SimpleVideoMatrix() {
			instance=this;
		}
		
		private var d2h: Array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F") ;
		private function bToHex(b: int): String {
			var h=(b>>4)&0x0f;
			var l=(b&0x0f);
			return d2h[h] + d2h[l];
		}
		private function baToHex(ba: ByteArray): String {
			ba.position=0;
			var str: String="";
			for(var i: int=0; i<ba.length; i++)
				str+=bToHex(ba.readByte());
			return str;
		}
		private function to2digits(i: int): String {
			var s: String=""+i;
			while(s.length<2)
				s = "0"+s;
			return s ;
		}
				
		public function fromXml(xml: XML): void {
			if(xml==null)
				return;
			useMeForComputerVideo=xml.useSvm=="true";
			address=xml.svmAddress;
			port=xml.svmPort;
			connect();
			
			currentOutPort = SharedObject.getLocal("SimpleVideoMatrix").data.currentOutPort;		
		}
		
		private function socketSend(ba: ByteArray): void {
			Logger.trase("SimpleVideoMatrix socketSend, data: " + baToHex(ba));
			ba.position=0;
			socket.writeBytes(ba, 0, ba.length);
			socket.flush();
		}
		
		private function connect(): void {
			if(address==null || port<=0)
				return;
			Logger.trase("SimpleVideoMatrix connect, address: " + address + ", port: " + port);
			socket.addEventListener(IOErrorEvent.IO_ERROR, onSocketError);
			socket.addEventListener(Event.CONNECT, onSocketConnected);
			socket.addEventListener(Event.CLOSE, onSocketClosed);
			socket.addEventListener(ProgressEvent.SOCKET_DATA, onSocketData);
			socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocketSecurityError);
			socket.connect(address, port);
		}
		
		private function onSocketSecurityError(obj) {
			Logger.trase("SimpleVideoMatrix onSocketSecurityError, obj: " + obj); 
			connected=false;
		}
		
		private function onSocketError(obj) {
			Logger.trase("SimpleVideoMatrix onSocketError,obj: " + obj); 
			connected=false;
			var t: Timer = new Timer(1000);
			t.repeatCount=1;
			t.addEventListener(TimerEvent.TIMER, function(event){
				connect();	
			});
			t.start();
		}
		
		private function onSocketConnected(obj) {
			Logger.trase("SimpleVideoMatrix onSocketConnected, obj: " + obj); 
			connected=true;
		}
		
		private function onSocketClosed(obj) {
			Logger.trase("SimpleVideoMatrix onSocketClosed, obj: " + obj); 
			connected=false;
			var t: Timer = new Timer(1000);
			t.repeatCount=1;
			t.addEventListener(TimerEvent.TIMER, function(event){
				connect();	
			});
			t.start();
		}
		
		private function onSocketData(obj) {
			var ba: ByteArray = new ByteArray() ;
			socket.readBytes(ba, 0, obj.bytesLoaded);
			ba.position=0;
			var s: String = new String(ba);
			Logger.trase("SimpleVideoMatrix onSocketData, data: " +  s);
		}
		
		public function getRoute(mxOutPort: int): int {
			return currentOutPort;
		}
		
		public function setRoute(mxInPort: int): void {
			Logger.trase("SimpleVideoMatrix setRoute, mxInport: " +  mxInPort);
			currentOutPort=mxInPort;
			SharedObject.getLocal("SimpleVideoMatrix").data.currentOutPort = currentOutPort;
			if(!connected)
				return;
			var ba: ByteArray=new ByteArray();
			var machine: int=0;
			var port: int=mxInPort-1;
			if(port>=15) {
				ba.writeByte(0x61);
				ba.writeByte(0x30+port-15);
				socketSend(ba);
			} else {
				ba.writeByte(0x60);
				ba.writeByte(0x30+port);
				socketSend(ba);
			}
		}
		

	}
}