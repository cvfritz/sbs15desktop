package {
	import flash.events.Event;

	public class SourceChangedEvent extends Event {
		
		[Bindable]
		public var newSource: Object;
		
		public function SourceChangedEvent(type:String, _newSource: Object) {
			super(type, false, false);
			newSource = _newSource;
		}
	}
}