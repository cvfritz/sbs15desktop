package {
	import flash.events.*;
	import flash.net.SharedObject;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	public class VideoMatrix2 extends EventDispatcher {
		
		public static var instance: VideoMatrix2;
		
		[Bindable]
		public var connected: Boolean=false;
		
		public var address: String="";
		public var port: int=0;
		public var useMeForComputerVideo: Boolean=false;
		public var computerPort: int=0;
		
		private var socket: Socket=null;
		private var videoRouteMap: Array = new Array();
		private var state: String="";
		private var timeoutTimer: Timer = new Timer(3000);
		
		public function VideoMatrix2() {
			instance=this;
		}
		
		private static var d2h: Array = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F") ;
		private function bToHex(b: int): String {
			var h: int=(b>>4)&0x0f;
			var l: int=(b&0x0f);
			return d2h[h] + d2h[l];
		}
		
		private function baToHex(ba: ByteArray): String {
			ba.position=0;
			var str: String="";
			for(var i: int=0; i<ba.length; i++)
				str+=bToHex(ba.readByte());
			return str;
		}
		
		private function to2digits(i: int): String {
			var s: String=""+i;
			while (s.length<2)
				s = "0"+s;
			return s ;
		}
		
		private function init(): void {	
			instance = this;
		}
		
		private function setState(s: String): void {
			Logger.trase("VideoMatrix2 setState, state: " + 	s);
			timeoutTimer.stop();
			state=s;
			if(state!="ready") 	{
				timeoutTimer.start();
			}
		}
		
		public function fromXml(xml: XML): void {
			if(xml==null)
				return;
			address=xml.address;
			port=xml.port;
			useMeForComputerVideo=xml.useMe=="true";
			computerPort=xml.computerPort;
			connect();
			updateRouteMap();
		}
		
		private function socketSend(ba: ByteArray): void {
			Logger.trase("VideoMatrix2 socketSend, data: " + baToHex(ba));
			ba.position=0;
			socket.writeBytes(ba, 0, ba.length)	;
			socket.flush();
		}
		
		private function connect(): void {
			if(address=="" || port<=0)
				return;
			Logger.trase("VideoMatrix2 connect, address: " + address + ", port: " + port);
			if(socket==null) {	
				socket = new Socket();
				socket.addEventListener(IOErrorEvent.IO_ERROR, onSocketError);
				socket.addEventListener(Event.CONNECT, onSocketConnected);
				socket.addEventListener(Event.CLOSE, onSocketClosed);
				socket.addEventListener(ProgressEvent.SOCKET_DATA, onSocketData);
				socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocketSecurityError);
			}
			socket.connect(address, port);
		}
		
		private function onSocketSecurityError(obj: Object): void {
			Logger.trase("VideoMatrix2 onSocketSecurityError, obj: " + obj); 
			connected=false;
		}
		
		private function onSocketError(obj: Object): void {
			Logger.trase("VideoMatrix2 onSocketError,obj: " + obj); 
			connected=false;
			var t: Timer = new Timer(1000);
			t.repeatCount=1;
			t.addEventListener(TimerEvent.TIMER, function(event: Event): void {
				connect();	
			});
			t.start();
		}
		
		private function onSocketConnected(obj: Object): void {
			Logger.trase("VideoMatrix2 onSocketConnected, obj: " + obj); 
			connected=true;
			setState("ready");
			updateRouteMap();
		}
		
		private function onSocketClosed(obj: Object): void {
			Logger.trase("VideoMatrix2onSocketClosed, obj: " + obj); 
			connected=false;
			var t: Timer = new Timer(1000);
			t.repeatCount=1;
			t.addEventListener(TimerEvent.TIMER, function(event: Event): void{
				connect();	
			});
			t.start();
		}
		
		private var buffer: String="";
		private function onSocketData(obj: Object): void {
			var ba: ByteArray = new ByteArray() ;
			socket.readBytes(ba, 0, obj.bytesLoaded);
			ba.position=0;
			var s: String = new String(ba);
			Logger.trase("VideoMatrix2 onSocketData, data: " + s);
			buffer += s;
			buffer = buffer.replace("\r	", "\n");
			while (true) {
				var t: 	int = buffer.indexOf("\n");
				if(t<0)
					break;
				var line: String = buffer.substr(0, t);
				line = line.replace("\n", "");
				buffer = buffer.substr(t+1);
				try {
					Logger.trase("VideoMatrix2 line: " + line);
					if(state=="waitForDone") {
						if(line=="DONE" || line=="VIDEO DONE") {
							Logger.trase("Ready");
							setState("ready");
							dispatchEvent(new Event("mapUpdated"));
						}
					} else	if (state=="waitForMap") {
						if(line=="DONE" || line=="VIDEO DONE") {
							setState("ready");
							dispatchEvent(new Event("mapUpdated"));
							continue;
							
						}
						if(line=="D")
							continue;
						
						line = line.replace("   OUTPUT", "");
						line = line.replace("  OUTPUT", "");
						line = line.replace(" OUTPUT", "");
						line = line.replace("OUTPUT", "");
						line = line.replace("VIDEO", "");
						line = line.replace("V", "");
						line = line.replace("    ", " ");
						line = line.replace("   ", " ");
						line = line.replace("  ", " ");
						line = line.replace("  ", " ");
						line = line.replace("  ", " ");
						line = line.replace("  ", " ");
						line = line.replace("  ", " ");
						
						var sa: Array = line.split(" ");
						
						var	 mxOutPort: String = sa[0];
						var mxInPort: String = sa[1];
						
						videoRouteMap[int(mxOutPort)]=int(mxInPort);
						SharedObject.getLocal("VideoMatrix2").data.videoRouteMap = videoRouteMap; 
						Logger.trase("VideoMatrix2 route " + mxInPort + " to " + mxOutPort); 
						
						if(mxInPort=="96") 	{
							setState("ready");
							dispatchEvent(new Event("mapUpdated"));
						}
					}
				} catch (e) {
					Logger.trase("VideoMatrix2 exception	: " + e);
				}
			}
		}
		
		public function getVideoRoute(mxOutPort: int): int {
			return videoRouteMap[mxOutPort];
		}
		
		public function setVideoRoute(mxInPort: int, mxOutPort: int): void {
			Logger.trase("VideoMatrix2 setVideoRoute, mxInport: " +  mxInPort + ", mxOutPort: " + mxOutPort);
			videoRouteMap[mxOutPort]=mxInPort;
			SharedObject.getLocal("VideoMatrix2").data.videoRouteMap = videoRouteMap;
			if(!connected)
				return;
			var ba:	ByteArray=new ByteArray();
			ba.writeMultiByte("B"+to2digits(mxOutPort)+to2digits(mxInPort)+"\r", "ASCII");
			ba.position=0;
			socketSend(ba);
			
			setState("waitForDone");
		}
		
		public function getClonedVideoRouteMap(): Array {
			var rm: Array = new Array(videoRouteMap.length);
			for(var i: int=0; i<rm.length; i++)
				rm[i]=videoRouteMap[i];
			return rm;
		}
		
		public function setVideoRouteMap(rm: Array): void {
			for(var i: int=0; i<rm.length; i++) {
				setVideoRoute(rm[i], i);
			}
		}
		
		public function updateRouteMap(): void {
			Logger.trase("VideoMatrix2 updateRouteMap");
			videoRouteMap = SharedObject.getLocal("VideoMatrix2").data.videoRouteMap;	
			if(videoRouteMap==null)
				videoRouteMap = new Array();
			SharedObject.getLocal("VideoMatrix2").data.videoRouteMap = videoRouteMap;
			if(!connected) {
				this.dispatchEvent(new Event("mapUpdated"));
				return;
			}
			var ba:	ByteArray=new ByteArray();
			ba.writeMultiByte("D\r", "ASCII");
			ba.position=0;
			socketSend(ba);	
			setState("waitForMap");
		}
		
		
	}
}