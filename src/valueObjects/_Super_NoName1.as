/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NoName1.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.Status;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NoName1 extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.Status.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _NoName1EntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_isRecording : Boolean;
    private var _internal_isPpv : Boolean;
    private var _internal_episodeTitle : String;
    private var _internal_status : valueObjects.Status;
    private var _internal_minor : int;
    private var _internal_isVod : Boolean;
    private var _internal_stationId : int;
    private var _internal_callsign : String;
    private var _internal_date : String;
    private var _internal_startTime : int;
    private var _internal_title : String;
    private var _internal_duration : int;
    private var _internal_programId : String;
    private var _internal_isOffAir : Boolean;
    private var _internal_isPclocked : int;
    private var _internal_rating : String;
    private var _internal_major : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NoName1()
    {
        _model = new _NoName1EntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "episodeTitle", model_internal::setterListenerEpisodeTitle));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "status", model_internal::setterListenerStatus));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "callsign", model_internal::setterListenerCallsign));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "date", model_internal::setterListenerDate));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "title", model_internal::setterListenerTitle));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "programId", model_internal::setterListenerProgramId));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "rating", model_internal::setterListenerRating));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get isRecording() : Boolean
    {
        return _internal_isRecording;
    }

    [Bindable(event="propertyChange")]
    public function get isPpv() : Boolean
    {
        return _internal_isPpv;
    }

    [Bindable(event="propertyChange")]
    public function get episodeTitle() : String
    {
        return _internal_episodeTitle;
    }

    [Bindable(event="propertyChange")]
    public function get status() : valueObjects.Status
    {
        return _internal_status;
    }

    [Bindable(event="propertyChange")]
    public function get minor() : int
    {
        return _internal_minor;
    }

    [Bindable(event="propertyChange")]
    public function get isVod() : Boolean
    {
        return _internal_isVod;
    }

    [Bindable(event="propertyChange")]
    public function get stationId() : int
    {
        return _internal_stationId;
    }

    [Bindable(event="propertyChange")]
    public function get callsign() : String
    {
        return _internal_callsign;
    }

    [Bindable(event="propertyChange")]
    public function get date() : String
    {
        return _internal_date;
    }

    [Bindable(event="propertyChange")]
    public function get startTime() : int
    {
        return _internal_startTime;
    }

    [Bindable(event="propertyChange")]
    public function get title() : String
    {
        return _internal_title;
    }

    [Bindable(event="propertyChange")]
    public function get duration() : int
    {
        return _internal_duration;
    }

    [Bindable(event="propertyChange")]
    public function get programId() : String
    {
        return _internal_programId;
    }

    [Bindable(event="propertyChange")]
    public function get isOffAir() : Boolean
    {
        return _internal_isOffAir;
    }

    [Bindable(event="propertyChange")]
    public function get isPclocked() : int
    {
        return _internal_isPclocked;
    }

    [Bindable(event="propertyChange")]
    public function get rating() : String
    {
        return _internal_rating;
    }

    [Bindable(event="propertyChange")]
    public function get major() : int
    {
        return _internal_major;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set isRecording(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRecording;
        if (oldValue !== value)
        {
            _internal_isRecording = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRecording", oldValue, _internal_isRecording));
        }
    }

    public function set isPpv(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isPpv;
        if (oldValue !== value)
        {
            _internal_isPpv = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isPpv", oldValue, _internal_isPpv));
        }
    }

    public function set episodeTitle(value:String) : void
    {
        var oldValue:String = _internal_episodeTitle;
        if (oldValue !== value)
        {
            _internal_episodeTitle = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "episodeTitle", oldValue, _internal_episodeTitle));
        }
    }

    public function set status(value:valueObjects.Status) : void
    {
        var oldValue:valueObjects.Status = _internal_status;
        if (oldValue !== value)
        {
            _internal_status = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "status", oldValue, _internal_status));
        }
    }

    public function set minor(value:int) : void
    {
        var oldValue:int = _internal_minor;
        if (oldValue !== value)
        {
            _internal_minor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "minor", oldValue, _internal_minor));
        }
    }

    public function set isVod(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isVod;
        if (oldValue !== value)
        {
            _internal_isVod = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isVod", oldValue, _internal_isVod));
        }
    }

    public function set stationId(value:int) : void
    {
        var oldValue:int = _internal_stationId;
        if (oldValue !== value)
        {
            _internal_stationId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "stationId", oldValue, _internal_stationId));
        }
    }

    public function set callsign(value:String) : void
    {
        var oldValue:String = _internal_callsign;
        if (oldValue !== value)
        {
            _internal_callsign = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "callsign", oldValue, _internal_callsign));
        }
    }

    public function set date(value:String) : void
    {
        var oldValue:String = _internal_date;
        if (oldValue !== value)
        {
            _internal_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "date", oldValue, _internal_date));
        }
    }

    public function set startTime(value:int) : void
    {
        var oldValue:int = _internal_startTime;
        if (oldValue !== value)
        {
            _internal_startTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "startTime", oldValue, _internal_startTime));
        }
    }

    public function set title(value:String) : void
    {
        var oldValue:String = _internal_title;
        if (oldValue !== value)
        {
            _internal_title = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "title", oldValue, _internal_title));
        }
    }

    public function set duration(value:int) : void
    {
        var oldValue:int = _internal_duration;
        if (oldValue !== value)
        {
            _internal_duration = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "duration", oldValue, _internal_duration));
        }
    }

    public function set programId(value:String) : void
    {
        var oldValue:String = _internal_programId;
        if (oldValue !== value)
        {
            _internal_programId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "programId", oldValue, _internal_programId));
        }
    }

    public function set isOffAir(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOffAir;
        if (oldValue !== value)
        {
            _internal_isOffAir = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOffAir", oldValue, _internal_isOffAir));
        }
    }

    public function set isPclocked(value:int) : void
    {
        var oldValue:int = _internal_isPclocked;
        if (oldValue !== value)
        {
            _internal_isPclocked = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isPclocked", oldValue, _internal_isPclocked));
        }
    }

    public function set rating(value:String) : void
    {
        var oldValue:String = _internal_rating;
        if (oldValue !== value)
        {
            _internal_rating = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rating", oldValue, _internal_rating));
        }
    }

    public function set major(value:int) : void
    {
        var oldValue:int = _internal_major;
        if (oldValue !== value)
        {
            _internal_major = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "major", oldValue, _internal_major));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerEpisodeTitle(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEpisodeTitle();
    }

    model_internal function setterListenerStatus(value:flash.events.Event):void
    {
        _model.invalidateDependentOnStatus();
    }

    model_internal function setterListenerCallsign(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCallsign();
    }

    model_internal function setterListenerDate(value:flash.events.Event):void
    {
        _model.invalidateDependentOnDate();
    }

    model_internal function setterListenerTitle(value:flash.events.Event):void
    {
        _model.invalidateDependentOnTitle();
    }

    model_internal function setterListenerProgramId(value:flash.events.Event):void
    {
        _model.invalidateDependentOnProgramId();
    }

    model_internal function setterListenerRating(value:flash.events.Event):void
    {
        _model.invalidateDependentOnRating();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.episodeTitleIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_episodeTitleValidationFailureMessages);
        }
        if (!_model.statusIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_statusValidationFailureMessages);
        }
        if (!_model.callsignIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_callsignValidationFailureMessages);
        }
        if (!_model.dateIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_dateValidationFailureMessages);
        }
        if (!_model.titleIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_titleValidationFailureMessages);
        }
        if (!_model.programIdIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_programIdValidationFailureMessages);
        }
        if (!_model.ratingIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ratingValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NoName1EntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NoName1EntityMetadata) : void
    {
        var oldValue : _NoName1EntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfEpisodeTitle : Array = null;
    model_internal var _doValidationLastValOfEpisodeTitle : String;

    model_internal function _doValidationForEpisodeTitle(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEpisodeTitle != null && model_internal::_doValidationLastValOfEpisodeTitle == value)
           return model_internal::_doValidationCacheOfEpisodeTitle ;

        _model.model_internal::_episodeTitleIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEpisodeTitleAvailable && _internal_episodeTitle == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "episodeTitle is required"));
        }

        model_internal::_doValidationCacheOfEpisodeTitle = validationFailures;
        model_internal::_doValidationLastValOfEpisodeTitle = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfStatus : Array = null;
    model_internal var _doValidationLastValOfStatus : valueObjects.Status;

    model_internal function _doValidationForStatus(valueIn:Object):Array
    {
        var value : valueObjects.Status = valueIn as valueObjects.Status;

        if (model_internal::_doValidationCacheOfStatus != null && model_internal::_doValidationLastValOfStatus == value)
           return model_internal::_doValidationCacheOfStatus ;

        _model.model_internal::_statusIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isStatusAvailable && _internal_status == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "status is required"));
        }

        model_internal::_doValidationCacheOfStatus = validationFailures;
        model_internal::_doValidationLastValOfStatus = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCallsign : Array = null;
    model_internal var _doValidationLastValOfCallsign : String;

    model_internal function _doValidationForCallsign(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCallsign != null && model_internal::_doValidationLastValOfCallsign == value)
           return model_internal::_doValidationCacheOfCallsign ;

        _model.model_internal::_callsignIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCallsignAvailable && _internal_callsign == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "callsign is required"));
        }

        model_internal::_doValidationCacheOfCallsign = validationFailures;
        model_internal::_doValidationLastValOfCallsign = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfDate : Array = null;
    model_internal var _doValidationLastValOfDate : String;

    model_internal function _doValidationForDate(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfDate != null && model_internal::_doValidationLastValOfDate == value)
           return model_internal::_doValidationCacheOfDate ;

        _model.model_internal::_dateIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isDateAvailable && _internal_date == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "date is required"));
        }

        model_internal::_doValidationCacheOfDate = validationFailures;
        model_internal::_doValidationLastValOfDate = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfTitle : Array = null;
    model_internal var _doValidationLastValOfTitle : String;

    model_internal function _doValidationForTitle(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfTitle != null && model_internal::_doValidationLastValOfTitle == value)
           return model_internal::_doValidationCacheOfTitle ;

        _model.model_internal::_titleIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isTitleAvailable && _internal_title == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "title is required"));
        }

        model_internal::_doValidationCacheOfTitle = validationFailures;
        model_internal::_doValidationLastValOfTitle = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfProgramId : Array = null;
    model_internal var _doValidationLastValOfProgramId : String;

    model_internal function _doValidationForProgramId(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfProgramId != null && model_internal::_doValidationLastValOfProgramId == value)
           return model_internal::_doValidationCacheOfProgramId ;

        _model.model_internal::_programIdIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isProgramIdAvailable && _internal_programId == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "programId is required"));
        }

        model_internal::_doValidationCacheOfProgramId = validationFailures;
        model_internal::_doValidationLastValOfProgramId = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfRating : Array = null;
    model_internal var _doValidationLastValOfRating : String;

    model_internal function _doValidationForRating(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfRating != null && model_internal::_doValidationLastValOfRating == value)
           return model_internal::_doValidationCacheOfRating ;

        _model.model_internal::_ratingIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isRatingAvailable && _internal_rating == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "rating is required"));
        }

        model_internal::_doValidationCacheOfRating = validationFailures;
        model_internal::_doValidationLastValOfRating = value;

        return validationFailures;
    }
    

}

}
